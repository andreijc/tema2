
/*C*/
select title
from sakila.film
where length > 150;

/*D'*/
select customer_id,first_name,last_name,count(*) as payments
from sakila.customer
inner join sakila.payment using(customer_id)
group by customer_id
having payments > 30;

/*E'*/
select customer_id,first_name,last_name,count(*) as payments
from sakila.customer
inner join sakila.payment using(customer_id)
group by customer_id
having payments > 30
order by payments desc
limit 3 offset 0;

/*G*/
update sakila.film_category
set category_id = 14
where film_id > 500;

select * from sakila.film_category;
rollback;

/*H*/
select *
from sakila.film
inner join(
	select film_id
	from sakila.film_actor
	where actor_id = (
		select actor_id
		from sakila.actor
		where first_name = 'JOHNNY' and last_name = 'CAGE')
) as actor_movies 
using(film_id);

/*I*/
select film_id,title,count(amount) as "Numar plati",sum(coalesce(amount,0)) as Total
from sakila.film 
left join sakila.inventory using(film_id)
left join sakila.rental using(inventory_id)
left join sakila.payment using(rental_id)
group by film_id;

/*J*/
select category_id,name,sum(coalesce(amount,0)) as Total
from sakila.category
left join sakila.film_category using(category_id)
left join sakila.film using(film_id)
left join sakila.inventory using(film_id)
left join sakila.rental using(inventory_id)
left join sakila.payment using(rental_id)
group by category_id
order by Total desc
limit 3;


